import React from 'react';
import HomePageComponent from './HomePageComponent';

class HomePageContainer extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <>
        <HomePageComponent title="DeÔnibus"/>
      </>
    );
  }
}

export default HomePageContainer;