/* eslint-disable import/no-named-as-default */
import React from "react";
import { Route, Switch } from "react-router-dom";
import HomePageContainer from "../components/HomePageContainer";
import PropTypes from "prop-types";
import { hot } from "react-hot-loader";

class App extends React.Component {
  render() {
    return (
      <>
        <Switch>
          <Route exact path="/" component={HomePageContainer} />
          {/* <Route component={NotFoundPage} /> */}
        </Switch>
      </>
    );
  }
}

App.propTypes = {
  children: PropTypes.element
};

export default hot(module)(App);